#!/usr/bin/env php
<?php

/*
   filesearchdb
   Copyright (C) 2022  Moritz Strohm <ncc1988@posteo.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


function makedb(PDO $db)
{
    $result = $db->exec(
        'CREATE TABLE filesearch_schemas (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL
        )'
    );
    if ($result === false) {
        fwrite(STDERR, "Error creating database (filesearch_schemas)!\n");
        exit(1);
    }

    $result = $db->exec(
        'CREATE TABLE files (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            path TEXT NOT NULL
        )'
    );
    if ($result === false) {
        fwrite(STDERR, "Error creating database (files)!\n");
        exit(1);
    }
}


function getSchemaData(PDO $db)
{
    $schema_data = $db->query('SELECT `id`, `name` FROM `filesearch_schemas`');
    if (!$schema_data) {
        fwrite(STDERR, "Database error!\n");
        exit(1);
    }
    return $schema_data->fetchAll(PDO::FETCH_ASSOC);
}


function getSchemaFields(PDO $db, int $schema_id, string $prefix = '') : array
{
    $result = $db->query(sprintf('PRAGMA table_info(filesearch_schema_%d)', $schema_id));
    if ($result === false) {
        return [];
    }
    $data = $result->fetchAll(PDO::FETCH_ASSOC);
    $fields = [];
    foreach ($data as $field_row) {
        if (in_array($field_row['name'], ['id', 'file_id'])) {
            //Skip the special fields.
            continue;
        }
        if ($prefix) {
            $fields[] = $prefix . '.' . $field_row['name'];
        } else {
            $fields[] = $field_row['name'];
        }
    }
    return $fields;
}


function splitAttributeString(string $attribute_string) : array
{
    $attribute = [];
    $key_and_value = explode('=', $attribute_string);
    if (count($key_and_value) != 2) {
        //Invalid key-value pair.
        return [];
    }
    $sanitised_key = str_replace(' ', '_', trim($key_and_value[0]));
    $attribute[$sanitised_key] = $key_and_value[1];
    return $attribute;
}


function splitAttributeList(string $attribute_list) : array
{
    $attributes = [];
    $items = explode(',', $attribute_list);
    foreach ($items as $item) {
        $item_data = splitAttributeString($item);
        if ($item_data) {
            $attributes = array_merge($attributes, $item_data);
        }
    }
    return $attributes;
}


function addFileToDatabase(PDO $db, array $file_attributes, string $file_path)
{
    $schema_data = getSchemaData($db);

    $all_fields = [];

    $found_attribute_c = 0;
    $found_attributes = [];
    foreach ($schema_data as $schema) {
        //Search all schemas for the attribute names:
        $schema_fields = getSchemaFields($db, $schema['id']);
        if ($schema_fields) {
            $all_fields[$schema['id']] = $schema_fields;
            //Determine where to place each attribute:
            foreach ($file_attributes as $key => $value) {
                if (in_array($key, $schema_fields)) {
                    if (empty($found_attributes[$schema['id']])) {
                        $found_attributes[$schema['id']] = [];
                    }
                    $found_attributes[$schema['id']][$key] = $value;
                    $found_attribute_c++;
                }
            }
        }
    }

    if ($found_attribute_c < count($file_attributes)) {
        fwrite(STDERR, "Error: At least one attribute does not belong to a schema!\n");
        exit(1);
    }

    $file_id = '';

    //Check if the file already exists:
    $file_id_stmt = $db->prepare('SELECT id FROM files WHERE path = :path');
    $file_id_stmt->execute(['path' => $file_path]);
    if (!($file_id = $file_id_stmt->fetchColumn())) {
        //The file must be created.
        $create_stmt = $db->prepare('INSERT INTO files (path) VALUES (:path)');
        $create_stmt->execute(['path' => $file_path]);
        $file_id_stmt->execute(['path' => $file_path]);
        $file_id = $file_id_stmt->fetchColumn();
        if (!$file_id) {
            fwrite(STDERR, "Error adding file!\n");
            exit(1);
        }
    }

    //Add the attributes to each schema:
    foreach ($found_attributes as $schema_id => $schema_attributes) {
        //Check if the schema already has an entry for the file:
        $entry_id_stmt = $db->prepare(sprintf('SELECT id FROM filesearch_schema_%u WHERE file_id = :file_id', $schema_id));
        $entry_id_stmt->execute(['file_id' => $file_id]);
        $entry_id = '';
        if ($entry_id = $entry_id_stmt->fetchColumn()) {
            //The file already has an entry in the schema table.
            //It must be updated.
            $fields_sql = [];
            foreach ($schema_attributes as $key => $value) {
                $fields_sql[] = sprintf('%1$s = %2$s', $db->quote($key), $db->quote($value));
            }
            $update_stmt_sql = sprintf(
                'UPDATE filesearch_schema_%1$u SET %2$s WHERE file_id = :file_id',
                $schema_id,
                implode(', ', $fields_sql)
            );
            $update_stmt = $db->prepare($update_stmt_sql);
            $result = $update_stmt->execute(['file_id' => $file_id]);
            if ($result === false) {
                fwrite(STDERR, "Error updating attributes!\n");
                exit(1);
            }
        } else {
            //Create the entry:
            $quoted_keys = [];
            $quoted_values = [];
            foreach ($schema_attributes as $key => $value) {
                $quoted_keys[] = $db->quote($key);
                $quoted_values[] = $db->quote($value);
            }
            $insert_stmt_sql = sprintf(
                'INSERT INTO filesearch_schema_%1$u (\'file_id\', %2$s) VALUES ( :file_id, %3$s )',
                $schema_id,
                implode(', ', $quoted_keys),
                implode(', ', $quoted_values)
            );
            $insert_stmt = $db->prepare($insert_stmt_sql);
            $result = $insert_stmt->execute(['file_id' => $file_id]);
            if ($result === false) {
                fwrite(STDERR, "Error adding attributes!\n");
                exit(1);
            }
        }
    }
    return true;
}

fwrite(STDERR, "filesearchdb  Copyright (C) 2022  Moritz Strohm\n"
             . "This is a prototype. Use with caution!\n\n");


if ($argc <= 1) {
    fwrite(
        STDERR,
        "filesearchdb usage: filesearchdb.php action [arguments] [file]\n"
        . "actions:\n"
      . "\tcreate:\tcreate database\n"
      . "\tschema:\thandle schema. Arguments: add: add schema name (name as second argument, fields with types comma separated as third argument), remove: remove schema (name as second argument)"
      . "\tfind:\tsearch database, conditions as argument string\n"
      . "\tadd:\tadd file. schema db fields as first argument (field1=value1,field2=value2), file as second argument\n"
    );
    exit(0);
}

$db = new PDO('sqlite:./filesearch.db');

$action = $argv[1];
if ($action == 'create') {
    makedb($db);
    fwrite(STDERR, "OK\n");
} elseif ($action == 'schema') {
    if ($argc < 5) {
        fwrite(STDERR, "Not enough parameters!");
        exit(1);
    }
    $schema_action = $argv[2];
    $schema_name = $argv[3];
    if ($schema_action == 'add') {
        $fields = splitAttributeList($argv[4]);
        if (!$schema_name || !$fields) {
            fwrite(STDERR, "No schema name or field spec given!\n");
            exit(1);
        }
        foreach ($fields as $field => $type) {
            if (!in_array($type, ['int', 'boolean', 'string'])) {
                //Invalid field type.
                fwrite(STDERR, sprintf('Error: field %1$s has an invalid type (%2$s).', $field, $type) . "\n");
                exit(1);
            }
        }

        $schema_id_stmt = $db->prepare('SELECT id FROM `filesearch_schemas` WHERE `name` = :schema_name');
        $schema_id_stmt->execute(['schema_name' => $schema_name]);
        if ($schema_id = $schema_id_stmt->fetchColumn()) {
            //The schema already exists.
            fwrite(STDERR, sprintf("Error: The schema %s already exists!\n", $schema_name));
            exit(1);
        }

        $stmt = $db->prepare('INSERT INTO `filesearch_schemas` (`name`) VALUES ( :schema_name )');
        $stmt->execute(['schema_name' => $schema_name]);

        //Get the schema-ID:
        $schema_id_stmt->execute(['schema_name' => $schema_name]);
        if (!($schema_id = $schema_id_stmt->fetchColumn())) {
            fwrite(STDERR, "Error inserting schema into schema list!\n");
            exit(1);
        }

        //Create a table for the schema:
        $table_sql = sprintf("CREATE TABLE filesearch_schema_%u (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            file_id INTEGER NOT NULL,
        ", $schema_id);
        $fields_sql = [];
        foreach ($fields as $name => $type) {
            $sql_type = 'varchar(512)';
            if ($type == 'int') {
                $sql_type = 'INTEGER';
            } elseif ($type == 'boolean') {
                $sql_type = 'INTEGER(1)';
            }
            $fields_sql[] = sprintf('%1$s %2$s NULL DEFAULT NULL', $name, $sql_type);
        }
        $table_sql .= implode(', ', $fields_sql);
        $table_sql .= ", FOREIGN KEY (file_id) REFERENCES files(id))";
        $db->exec($table_sql);
    }
    fwrite(STDERR, "OK\n");
} elseif ($action == 'query') {
    if ($argc < 2) {
        fwrite(STDERR, "Not enough parameters!");
        exit(1);
    }
    $attributes = [];
    for ($i = 2; $i < $argc; $i++) {
        $attribute = splitAttributeString($argv[$i]);
        if ($attribute) {
            $attributes = array_merge($attributes, $attribute);
        }
    }
    $schemas = getSchemaData($db);
    if (!$schemas) {
        fwrite(STDERR, "No filesearch schema!\n");
        exit(1);
    }

    $found_attributes = [];
    $found_attribute_c = 0;
    foreach ($schemas as $schema) {
        $schema_fields = getSchemaFields($db, $schema['id']);
        if (!$schema_fields) {
            continue;
        }
        foreach ($attributes as $key => $value) {
            if (in_array($key, $schema_fields)) {
                if (empty($found_attributes[$schema['id']])) {
                    $found_attributes[$schema['id']] = [];
                }
                $found_attributes[$schema['id']][$key] = $value;
                $found_attribute_c++;
            }
        }
    }
    if ($found_attribute_c < count($attributes)) {
        fwrite(STDERR, "Error: At least one attribute does not belong to a schema!\n");
        exit(1);
    }

    //Query each schema for files matching the attributes:
    $all_file_paths = [];
    foreach ($found_attributes as $schema_id => $fields_to_query) {
        $fields_sql = [];
        foreach ($fields_to_query as $key => $value) {
            $fields_sql[] = sprintf('%1$s = %2$s', str_replace(' ', '_', trim($key)), $db->quote($value));
        }
        $query_sql = sprintf(
            'SELECT files.path FROM files INNER JOIN filesearch_schema_%1$u
            ON files.id = filesearch_schema_%1$s.file_id
            WHERE %2$s',
            $schema_id,
            implode(' AND ', $fields_sql)
        );
        $stmt = $db->prepare($query_sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        if ($result) {
            $all_file_paths = array_merge($all_file_paths, $result);
        }
    }
    if ($all_file_paths) {
        $all_file_paths = array_unique($all_file_paths);
        foreach ($all_file_paths as $path) {
            fwrite(STDOUT, escapeshellarg($path) . "\n");
        }
    }
} elseif ($action == 'add') {
    if ($argc < 4) {
        fwrite(STDERR, "Not enough parameters!");
        exit(1);
    }
    $file_attributes = [];
    //Use each parameter until the last one as key-value pair for attributes:
    for ($i = 2; $i < ($argc - 1); $i++) {
        $file_attribute = splitAttributeString($argv[$i]);
        if ($file_attribute) {
            $file_attributes = array_merge($file_attributes, $file_attribute);
        }
    }
    $file_path = $argv[$argc - 1];
    if (file_exists($file_path)) {
        //Add the file to the database:
        if (!addFileToDatabase($db, $file_attributes, $file_path)) {
            fwrite(STDERR, sprintf("An error occurred while adding file %s to the database!\n", $file_path));
            exit(1);
        }
    } else {
        fwrite(STDERR, sprintf("Error: File %s does not exist!\n", $file_path));
        exit(1);
    }
    fwrite(STDERR, "OK\n");
}
